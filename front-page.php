<?php
/**
 * Template Name: Home
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// get journals post type array
$context['recent_journals'] = Timber::get_posts([
	'post_type' => 'journal',
	'posts_per_page' => 3,
	'orderby' => 'date',
	'order' => 'DESC' // most recent first
]);

$templates = [ 'front-page.twig' ];

Timber::render( $templates, $context );