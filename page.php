<?php
/**
 * Default Template.
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$current = $post->ID;
$parent = $post->post_parent;
$grandparent_get = get_post($parent);
$grandparent = $grandparent_get->post_parent;

// check page hierarchy for parents and grandparent titles to show the parent title
if( $root_parent = get_the_title($grandparent) !== $root_parent = get_the_title($current) ) {
	$context['parent'] = get_the_title($grandparent);
} else {
	$context['parent'] = get_the_title($parent);
}

/**
	if the title obtained above is the current page (e.g. this page IS the parent) -
	overwrite parent context to show the site root as the parent
*/
if( get_the_title($parent) == get_the_title($current) ) {
	$context['parent'] = 'Salina Family Health Center';
}

$templates = [ 'page.twig' ];

Timber::render( $templates, $context );