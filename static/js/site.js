(function($) {
    $(document).ready(function() {
		let $body = $('body');

		$(function siteNavigation() {
			// menu open/close functions
			$('#menu-toggle').click(function() {
				$('.x-bar').toggleClass('x-bar-active');
				$('#primary-menu').fadeToggle(400);

				$('.nav-wrapper').toggleClass('lock-nav');
				$('body').toggleClass('lock-body');
			});
		});

		// mobile nav core functions - sub-menu open/expand functions and handlers for back button option
		$(function mobileNavigationRouter() {
			if( $body.width() < 851) {
				// add back button at top of each sub-menu
				$('.sub-menu').prepend('<li><button id="mobile-back-button" class="menu-back"><span class="fas fa-arrow-left"></span>Back</button></li>');

				// close sub-menu when back button is clicked
				$('li button.menu-back').on('click', function(e) {
					var $menu = $(this).parents('.sub-menu:first');
					$menu.removeClass('sub-menu-open');
					e.stopPropagation(); // prevent event from bubbling back up
				});

				// expand function
				$('.menu-item-has-children').click(function(e) {
					var $sub = $(this).find('.sub-menu:first');

					// if it has children, but doesn't have it's menu open, do not follow the link
					if( ! $sub.hasClass('sub-menu-open')) {
						e.preventDefault(); // don't follow links on first click
					}

					$sub.addClass('sub-menu-open');
					e.stopPropagation(); // prevent from bubbling back up
				});
			}
		});

		// dropdown block - controls and aria attributes included for screenreaders
		$(function dropDownBlock() {
			$('.dropdown-content').each(function() {
				$(this).hide();
			});

			$('.dropdown-block-title').click(function() {
				var $this = $(this);
				// fires on first click (content is expanded)
				if( $this.hasClass('target') ) {
					$this.removeClass('target');
					$this.next('.dropdown-content').slideToggle(700);
					$this.attr('aria-pressed', 'true');
					$this.next('.dropdown-content').attr('aria-expanded', 'true');
				} else {
					// fires on second click (content is closed)
					$this.next('.dropdown-content:first').slideToggle(700, function() {
						$this.prev('.dropdown-block-title').addClass('target');
						$this.prev('.dropdown-block-title').attr('aria-pressed', 'false');
						$this.attr('aria-expanded', 'false');
					});
				}
				// always fire
				$(this).toggleClass('chevron-rotate');
			});
		});

	}); // end Document.Ready
})(jQuery);