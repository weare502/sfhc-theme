mapboxgl.accessToken = 'pk.eyJ1Ijoiam9yZGFuNTAyIiwiYSI6ImNqZWVoZmQ3aTI5MWQyd28xMDZiN3R5aXcifQ.IpeRF59rJ3j7A4FPEGp5SA';
var map = new mapboxgl.Map({
	container: 'map', // div#map
	style: 'mapbox://styles/jordan502/ckek9g45u04w71apc26azlls1'
});

map.scrollZoom.disable(); // allows user to scroll/swipe without zooming (still allows pan x/y)
map.addControl(new mapboxgl.NavigationControl());