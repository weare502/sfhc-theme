<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// clinic parent/child setup
// to obtain parent content on child pages, we need to check if the post is a child or a parent-
// as the calls for data are different depending on hierarchy (we are displaying parent post data on child posts)
if( is_singular('clinic') ) :
	if( $post->post_parent != 0 ) {
		$context['is_child'] = true;
		$context['child_is_closed'] = MBHIPRO()->is_closed( get_the_title( $post->post_parent ) );
	} else {
		$context['is_parent'] = true;
		$context['is_closed'] = MBHIPRO()->is_closed( $post->title );
	}
endif;

$context['directory'] = Timber::get_posts([
	'post_type' => 'directory',
	'posts_per_page' => -1,
	'meta_key' => 'last_name',
	'orderby' => 'meta_value',
	'order' => 'ASC'
]);

the_post();

Timber::render( [ 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ], $context );