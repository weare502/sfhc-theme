<?php
/**
 * Template Name: Directory Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['directory'] = Timber::get_posts([
	'post_type' => 'directory',
	'posts_per_page' => 12, // controls pager facet "per page" output
	'meta_key' => 'last_name',
	'orderby' => 'meta_value',
	'order' => 'ASC',
	'facetwp' => true
]);

$templates = [ 'archive-directory.twig' ];

Timber::render( $templates, $context );