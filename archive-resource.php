<?php
/**
 * Template Name: Resource Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['resources'] = Timber::get_posts([
	'post_type' => 'resource',
	'posts_per_page' => -1,
	'orderby' => 'date',
	'order' => 'DESC',
	'facetwp' => true
]);

$templates = [ 'archive-resource.twig' ];

Timber::render( $templates, $context );