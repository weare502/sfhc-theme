<?php

// change 'views' directory to 'templates'
Timber::$locations = __DIR__ . '/templates';

class SFHCSite extends TimberSite {

	function __construct() {
		// Action Hooks //
		add_action( 'after_setup_theme', [ $this, 'after_setup_theme' ] );
		add_action( 'after_setup_theme', [ $this, 'sfhc_color_palette' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
		add_action( 'enqueue_block_assets', [ $this, 'backend_frontend_styles' ] );
		add_action( 'admin_head', [ $this, 'admin_head_css' ] );
		add_action( 'admin_menu', [ $this, 'admin_menu_cleanup'] );
		add_action( 'init', [ $this, 'register_post_types' ] );
		add_action( 'acf/init', [ $this, 'render_custom_acf_blocks' ] );
		add_action( 'login_enqueue_scripts', [ $this, 'style_login' ] );

		// Filter Hooks //
		add_filter( 'timber_context', [ $this, 'add_to_context' ] );
		add_filter( 'gform_enable_field_label_visibility_settings', [ $this, '__return_true' ] );
		add_filter( 'block_categories', [ $this, 'sfhc_block_category' ], 10, 2 );

		// Comment Column Removal //
		add_filter( 'manage_edit-page_columns', [ $this, 'disable_admin_columns' ] );

		parent::__construct();
	}

	// hide admin area unused features / notices
	function admin_head_css() {
		?><style type="text/css">
			#wp-admin-bar-comments { display: none !important; }
			.update-nag { display: none !important; }

			/* Hide annoying ManageWP popup */
			.mwp-notice-container { display: none !important; }

			#menu-posts-journal { margin-top: 0.75rem !important; }
		</style><?php
	}

	// WP admin login styles
	function style_login() {
		?><style type="text/css">
			#login h1, .login h1 {
				padding: 0.75rem 0.5rem;
				border-radius: 2px;
			}

			#login h1 a, .login h1 a {
				background-image: url(<?= get_stylesheet_directory_uri(); ?>/static/images/sfhc-logo.png);
				background-position: center;
				width: 9rem;
				height: 3rem;
				background-size: contain;
				padding: 1rem 4rem;
				margin: 0 auto;
			}
		</style><?php
	}

	function enqueue_scripts() {
		$version = filemtime( get_stylesheet_directory() . '/style-dist.css' );
		wp_enqueue_style( 'sfhc-css', get_stylesheet_directory_uri() . '/style-dist.css', [], $version );

		// jquery site js (main)
		wp_enqueue_script( 'sfhc-js', get_template_directory_uri() . '/static/js/site-dist.js', ['jquery'], $version );

		// mapbox api register (Mapbox GL JS) - only used on contact page $post->ID = 98
		if( is_page(98) ) {
			wp_register_script( 'mapbox', 'https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js' );
			wp_enqueue_script( 'mapbox' );
		}
	}

	// Uses the 'enqueue_block_assets' hook
	function backend_frontend_styles() {
		wp_enqueue_style( 'blocks-css', get_stylesheet_directory_uri() . '/block-style-dist.css' );
	}

	// Custom Timber context helper functions
	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['date'] = date('F j, Y');
		$context['date_year'] = date('Y');
		$context['options'] = get_fields('option');
		$context['home_url'] = home_url('/');
		$context['is_front_page'] = is_front_page();
		$context['get_url'] = $_SERVER['REQUEST_URI'];

		return $context;
	}

	// Menus / Theme Support / ACF Options Page
	function after_setup_theme() {
		register_nav_menu( 'primary', 'Site Navigation' );
		register_nav_menu( 'footer', 'Footer Menu' );

		add_theme_support( 'menus' );
		add_theme_support( 'align-wide' );
		add_theme_support( 'post-thumbnails' );

		if( function_exists('acf_add_options_page') ) {
			acf_add_options_page([
				'page_title' => 'Global Site Data',
				'menu_title' => 'Global Site Data',
				'capability' => 'edit_posts',
				'redirect' => false,
				'updated_message' => 'Global options updated'
			]);
		}
	}

	// registers and renders our custom acf blocks
	function render_custom_acf_blocks() {
		require 'acf-block-functions.php';
	}

	// creates a custom category for our theme-specific blocks
	function sfhc_block_category( $categories, $post ) {
		return array_merge( $categories,
			[[
				'slug' => 'sfhc-blocks',
				'title' => 'SFHC Blocks'
			],
		]);
	}

	// get rid of clutter
	function disable_admin_columns( $columns ) {
		unset( $columns['comments'] );
		return $columns;
	}

	function admin_menu_cleanup() {
		remove_menu_page( 'edit.php' ); // Posts
		remove_menu_page( 'edit-comments.php' ); // Comments
	}

	// add cpts here
	function register_post_types() {
		include_once('custom-post-types/post-type-journal.php');
		include_once('custom-post-types/post-type-directory.php');
		include_once('custom-post-types/post-type-resource.php');
		include_once('custom-post-types/post-type-clinic.php');
	}

	function sfhc_color_palette() {
		add_theme_support( 'disable-custom-colors' );
		$colors = [
			[
				// Purple
				'name' => __( 'Purple', 'sfhc' ),
				'slug' => 'purple',
				'color' => '#4A2B74'
			],

			[
				// Mint
				'name' => __( 'Teal', 'sfhc' ),
				'slug' => 'teal',
				'color' => '#32926A'
			],

			[
				// Light Black
				'name' => __( 'Light Black', 'sfhc' ),
				'slug' => 'grey',
				'color' => '#2F2F2F'
			]
		];
		add_theme_support( 'editor-color-palette', $colors );
	}
} // End of SFHCSite class

new SFHCSite();

// main site nav
function sfhc_render_primary_menu() {
	wp_nav_menu([
		'theme_location' => 'primary',
		'container' => false,
		'menu_id' => 'primary-menu'
	]);
}

// footer nav (external links)
function sfhc_render_footer_menu() {
	wp_nav_menu([
		'theme_location' => 'footer',
		'container' => false,
		'menu_id' => 'footer-menu'
	]);
}

// multi-select resource post object (Resource block picker)
function resource_block_picker() {
	$resource_objects = get_field('resources_picker'); ?>

	<div class="resources-grid add-mtb"> <?php
		if( $resource_objects ) :
			foreach( $resource_objects as $resource ) :
				$pid = $resource->ID;
				$title = get_the_title( $pid );
				$pdf_or_website = get_field( 'pdf_or_website', $pid );
				$pdf = get_field( 'pdf_upload', $pid );
				$website = get_field( 'website_url', $pid );
				$terms = get_the_terms( $pid, 'language' );
				?>

				<article class="resources-grid__post">
					<h4><?= $title; ?></h4> <?php

					if( $pdf_or_website ) : // default value is PDF ?>
						<p>PDF <span>&middot;</span><?php foreach($terms as $t): echo ' ' . $t->name; endforeach; ?></p> <!-- Category -->
						<a class="plain-link" href="<?= $pdf['url']; ?>" target="_blank" rel="nofollow">View Document</a>
					<?php else : ?>
						<p>Website</p>
						<a class="plain-link" href="<?= $website['url']; ?>" target="_blank" rel="nofollow">View Website</a>
					<?php endif; ?>
				</article>
			<?php endforeach;
		endif; ?>
	</div> <?php
}

// hide facet counts site-wide
add_filter( 'facetwp_facet_dropdown_show_counts', '__return_false' );

// run if  _wp_page_template  is not empty (custom template is used)
// for the Default Template it will be empty. (default is used when no template is set)
// Code Courtesy of: Bill Erickson
function ea_disable_editor( $id = false ) {
	$excluded_templates = [
		'front-page.php',
		'contact.php'
	];

	if( empty( $id ) )
		return false;

	$id = intval( $id );
	$template = get_page_template_slug( $id );

	return in_array( $template, $excluded_templates );
}

function ea_disable_gutenberg( $can_edit, $post_type ) {
	if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
		return $can_edit;

	if( ea_disable_editor( $_GET['post'] ) )
		$can_edit = false;

	return $can_edit;
}
add_filter( 'gutenberg_can_edit_post_type', 'ea_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2 );

// move our ACF Options Page (Global Site Data) below the Dashboard tab
function custom_menu_order( $menu_ord ) {  
    if ( ! $menu_ord ) {
		return true;
	}

	$menu = 'acf-options-global-site-data';

    // remove from current menu
    $menu_ord = array_diff( $menu_ord, [$menu] );

    // append after index [0]
    array_splice( $menu_ord, 1, 0, [$menu] );

    return $menu_ord;
}
add_filter( 'custom_menu_order', 'custom_menu_order' );
add_filter( 'menu_order', 'custom_menu_order' );

// hide editor on certain template pages
function hide_editor() {
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    if( !isset( $post_id ) ) return;

    $template_file = get_post_meta( $post_id, '_wp_page_template', true );

	// no editor on home or contact page (not used)
    if( $template_file == 'front-page.php' || $template_file == 'contact.php' ) {
        remove_post_type_support( 'page', 'editor' );
    }
}
add_action( 'admin_init', 'hide_editor' );






/**
 * Gravity Perks // GP Limit Submissions // Top Level Validation Message for Hidden Fields
 * http://gravitywiz.com/documentation/gravity-forms-limit-submissions/
 *
 * If a Limit Submissions feed returns a validation error and is based on the field values of a hidden field, the
 * feed's Limit Message will be used as the form's top level validation message.
 */
add_filter( 'gform_validation_message', function ( $message, $form ) {

	$has_other_error  = false;
	$gpls_error_field = false;

	foreach ( $form['fields'] as $field ) {

		if ( ! $field->failed_validation ) {
			continue;
		}

		if ( ( $field->visibility === 'hidden' || $field->get_input_type() === 'hidden' ) && strpos( $field->validation_message, 'gpls-limit-message' ) ) {
			$gpls_error_field = $field;
		} else {
			$has_other_error = true;
		}
	}

	if ( $gpls_error_field && ! $has_other_error ) {
		$message = sprintf( "<div class='validation_error'>%s</div>", $gpls_error_field->validation_message );
	}

	return $message;
}, 10, 2 );