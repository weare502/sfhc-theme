<?php
/**
 * Template Name: Journal Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// get journals post type array
$context['journals'] = Timber::get_posts([
	'post_type' => 'journal',
	'posts_per_page' => -1,
	'orderby' => 'date',
	'order' => 'DESC',
	'facetwp' => true
]);

$templates = [ 'archive-journal.twig' ];

Timber::render( $templates, $context );