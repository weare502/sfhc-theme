<?php
// Directory Posts
$labels = [
	'name'               => __( 'Directory', 'sfhc' ),
	'singular_name'      => __( 'Directory', 'sfhc' ),
	'add_new'            => _x( 'Add Person', 'sfhc', ),
	'add_new_item'       => __( 'Add Person', 'sfhc' ),
	'edit_item'          => __( 'Edit Person', 'sfhc' ),
	'new_item'           => __( 'New Person', 'sfhc' ),
	'view_item'          => __( 'View Directory', 'sfhc' ),
	'search_items'       => __( 'Search Directory', 'sfhc' ),
	'not_found'          => __( 'No People found', 'sfhc' ),
	'not_found_in_trash' => __( 'No People found in Trash', 'sfhc' ),
	'parent_item_colon'  => __( 'Parent Directory:', 'sfhc' ),
	'menu_name'          => __( 'Directory', 'sfhc' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [ 'emp_department' ],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-businessman',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // Directory Archive
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title', 'thumbnail' ],
];
register_post_type( 'directory', $args );

// setup taxonomy for department filtering
$tax_labels = [
	'name' 				=> 'Departments',
	'singular_name' 	=> 'Department',
	'search_items' 		=> 'Search ' . 'Department',
	'all_items' 		=> 'All ' . 'Departments',
	'edit_item' 		=> 'Edit ' . 'Department',
	'update_item' 		=> 'Update ' . 'Department',
	'add_new_item' 		=> 'Add New ' . 'Department',
	'new_item_name' 	=> 'New ' . 'Department',
	'menu_name' 		=> 'Departments',
	'parent_item'		=> 'Parent Department',
];

$tax_args = [
	'hierarchical' 	    => true,
	'labels' 	    	=> $tax_labels,
	'show_ui' 	    	=> true,
	'show_admin_column' => true,
	'has_archive'		=> false,
	'query_var'	    	=> true,
	'show_in_rest'		=> true, // required to show categories in Block Editor sidebar
	'rewrite'			=> $rewrite,
];
register_taxonomy( 'emp_department', 'directory', $tax_args );