<?php
// Clinic Posts (Hierarchical)
$labels = [
	'name'               => __( 'Clinics', 'sfhc' ),
	'singular_name'      => __( 'Clinic', 'sfhc' ),
	'add_new'            => _x( 'Add Clinic', 'sfhc', ),
	'add_new_item'       => __( 'Add Clinic', 'sfhc' ),
	'edit_item'          => __( 'Edit Clinic', 'sfhc' ),
	'new_item'           => __( 'New Clinic', 'sfhc' ),
	'view_item'          => __( 'View Clinic', 'sfhc' ),
	'search_items'       => __( 'Search Clinics', 'sfhc' ),
	'not_found'          => __( 'No Clinics found', 'sfhc' ),
	'not_found_in_trash' => __( 'No Clinics found in Trash', 'sfhc' ),
	'parent_item_colon'  => __( 'Parent Clinic:', 'sfhc' ),
	'menu_name'          => __( 'Clinics', 'sfhc' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-clipboard',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // Directory Archive
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'editor', 'title', 'page-attributes' ],
];
register_post_type( 'clinic', $args );