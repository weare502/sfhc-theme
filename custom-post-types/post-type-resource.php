<?php
// Resource Posts (pdf uploads + excerpt)
$labels = [
	'name'               => __( 'Resources', 'sfhc' ),
	'singular_name'      => __( 'Resource', 'sfhc' ),
	'add_new'            => _x( 'Add Resource', 'sfhc', ),
	'add_new_item'       => __( 'Add Resource', 'sfhc' ),
	'edit_item'          => __( 'Edit Resource', 'sfhc' ),
	'new_item'           => __( 'New Resource', 'sfhc' ),
	'view_item'          => __( 'View Resources', 'sfhc' ),
	'search_items'       => __( 'Search Resources', 'sfhc' ),
	'not_found'          => __( 'No Resources found', 'sfhc' ),
	'not_found_in_trash' => __( 'No Resources found in Trash', 'sfhc' ),
	'parent_item_colon'  => __( 'Parent Resource:', 'sfhc' ),
	'menu_name'          => __( 'Resources', 'sfhc' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [ 'language' ],
	'public'              => false,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-pdf',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // Journal Archive
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title' ],
];
register_post_type( 'resource', $args );

// setup taxonomy for department filtering
$tax_labels = [
	'name' 				=> 'Languages',
	'singular_name' 	=> 'Language',
	'search_items' 		=> 'Search ' . 'Language',
	'all_items' 		=> 'All ' . 'Languages',
	'edit_item' 		=> 'Edit ' . 'Language',
	'update_item' 		=> 'Update ' . 'Language',
	'add_new_item' 		=> 'Add New ' . 'Language',
	'new_item_name' 	=> 'New ' . 'Language',
	'menu_name' 		=> 'Languages',
	'parent_item'		=> NULL, // remove parent items to prevent breaking if someone adds one
];

$tax_args = [
	'hierarchical' 	    => true,
	'labels' 	    	=> $tax_labels,
	'show_ui' 	    	=> true,
	'show_admin_column' => true,
	'has_archive'		=> false,
	'query_var'	    	=> true,
	'show_in_rest'		=> true, // required to show categories in Block Editor sidebar
	'rewrite'			=> $rewrite,
];
register_taxonomy( 'language', 'resource', $tax_args );