<?php
// Journal Posts (pdf uploads + excerpt)
$labels = [
	'name'               => __( 'Journal', 'sfhc' ),
	'singular_name'      => __( 'Journal', 'sfhc' ),
	'add_new'            => _x( 'Add Journal', 'sfhc', ),
	'add_new_item'       => __( 'Add Journal', 'sfhc' ),
	'edit_item'          => __( 'Edit Journal', 'sfhc' ),
	'new_item'           => __( 'New Journal', 'sfhc' ),
	'view_item'          => __( 'View Journals', 'sfhc' ),
	'search_items'       => __( 'Search Journals', 'sfhc' ),
	'not_found'          => __( 'No Journals found', 'sfhc' ),
	'not_found_in_trash' => __( 'No Journals found in Trash', 'sfhc' ),
	'parent_item_colon'  => __( 'Parent Journal:', 'sfhc' ),
	'menu_name'          => __( 'Journals', 'sfhc' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [ 'department', 'audience' ],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-edit-large',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // Journal Archive
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title', 'editor', 'thumbnail' ],
];
register_post_type( 'journal', $args );

// setup taxonomy for department filtering
$tax_labels = [
	'name' 				=> 'Departments',
	'singular_name' 	=> 'Department',
	'search_items' 		=> 'Search ' . 'Department',
	'all_items' 		=> 'All ' . 'Departments',
	'edit_item' 		=> 'Edit ' . 'Department',
	'update_item' 		=> 'Update ' . 'Department',
	'add_new_item' 		=> 'Add New ' . 'Department',
	'new_item_name' 	=> 'New ' . 'Department',
	'menu_name' 		=> 'Departments',
	'parent_item'		=> NULL, // remove parent items to prevent breaking if someone adds one
];

$tax_args = [
	'hierarchical' 	    => false,
	'labels' 	    	=> $tax_labels,
	'show_ui' 	    	=> true,
	'show_admin_column' => true,
	'has_archive'		=> false,
	'query_var'	    	=> true,
	'show_in_rest'		=> true, // required to show categories in Block Editor sidebar
	'rewrite'			=> $rewrite,
];
register_taxonomy( 'department', 'journal', $tax_args );

// setup taxonomy for audience filtering
$tax_labels_a = [
	'name' 				=> 'Audiences',
	'singular_name' 	=> 'Audience',
	'search_items' 		=> 'Search ' . 'Audience',
	'all_items' 		=> 'All ' . 'Audiences',
	'edit_item' 		=> 'Edit ' . 'Audience',
	'update_item' 		=> 'Update ' . 'Audience',
	'add_new_item' 		=> 'Add New ' . 'Audience',
	'new_item_name' 	=> 'New ' . 'Audience',
	'menu_name' 		=> 'Audiences',
	'parent_item'		=> NULL, // remove parent items to prevent breaking if someone adds one
];

$tax_args_a = [
	'hierarchical' 	    => true,
	'labels' 	    	=> $tax_labels_a,
	'show_ui' 	    	=> true,
	'show_admin_column' => true,
	'has_archive'		=> false,
	'query_var'	    	=> true,
	'show_in_rest'		=> true, // required to show categories in Block Editor sidebar
	'rewrite'			=> $rewrite,
];
register_taxonomy( 'audience', 'journal', $tax_args_a );